# 洛谷插件镜像

#### 介绍

本仓库是为方便中国用户高效访问洛谷插件的仓库。


#### 使用说明

1.  找到你需要的插件，进入插件文件夹
2.  选择你需要的插件版本
3.  点击“原始数据”，进行安装

#### 参与贡献

1.  Star 本仓库
3.  提交 Issue
4.  新建 Pull Request

#### 仓库镜像

[Gitlab](https://gitlab.com/tim1103/luogu-plugin-mirrors/) | [Gitee](https://gitee.com/tim1103/Luogu-Plugin-Mirrors/)

#### 免责声明

插件均为在插件市场公开发布的插件，请严格按照《洛谷用户协议》使用。如出现封号等不可抗力原因，tim1103不负责任。（如果要找人负责那为啥不去找插件开发人呢）
![](https://images.gitee.com/uploads/images/2020/0717/103307_9fa16553_4920289.jpeg)